
    
    

select
    CUSTOMER_SK as unique_field,
    count(*) as n_records

from GLOBAL_DEV.DW_SALES.DIM_CUSTOMER
where CUSTOMER_SK is not null
group by CUSTOMER_SK
having count(*) > 1


