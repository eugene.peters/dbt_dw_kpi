
    
    

select
    NATION_SK as unique_field,
    count(*) as n_records

from GLOBAL_DEV.DW_SALES.DIM_NATION
where NATION_SK is not null
group by NATION_SK
having count(*) > 1


