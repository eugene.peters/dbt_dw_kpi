
    
    

select
    REGION_SK as unique_field,
    count(*) as n_records

from GLOBAL_DEV.DW_SALES.DIM_REGION
where REGION_SK is not null
group by REGION_SK
having count(*) > 1


